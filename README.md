
### Learn uWSGI

#### References

https://www.digitalocean.com/community/tutorials/how-to-set-up-uwsgi-and-nginx-to-serve-python-apps-on-centos-7

Tutorial is based on CentOS (However Ubuntu worked better)

#### Create Docker 

```
docker run -it --name uwsgiu20 ubuntu:20.04
```

```
apt update
apt install python3
apt install python3-pip
apt install python3-all-dev
pip install virtualenv
```

Create myapp and wsgi.py per Tutorial.

docker run -it -p 8080:8080 --rm  uwsgiu20:v1
cd ~/myapp/
source myappenv/bin/activat
uwsgi --socket 0.0.0.0:8080 --protocol=http -w wsgi

**NOTE:** Python3 Wants Bytes instead of String 

```
def application(environ, start_response):
    start_response('200 OK', [('Content-Type', 'text/html')])
    return [b"<h1 style='color:blue'>Hello There!</h1>"]
```

#### Create Image 

You can create image from Runtime; however, Dockerfile is more standard way.

```
docker commit 820e408e4928 uwsgiu20:v1
```

#### Create Dockerfile with basic application
- Don't think virtualenv is needed (Each pod would have a container for single Python app)
- Apps could be exposed on port 80 of unique svc name (load balanced) in Kubernetes


