### Learning Django 

#### Python/Django/VirtualEnv Installed

On my Mac python3 was starting python3.8; "which" showed python3 was /usr/bin/python3 (3.8.9)

The homebrew installed Python 3.9 worked better.  The pip3 acutally installed django and virtualenv.

On My system: 

```
sudo ln -s /opt/homebrew/bin/pip3.9 /usr/local/bin/pip3
sudo ln -s /opt/homebrew/bin/python3.9 /usr/local/bin/python3
```

Now python3 starts Python3.9

I was able to install virutalenv

```
pip3 install virtualenv 
```

```
virtualenv thanos
cd thanos
source bin/activate
pip3 install django
django-admin
python  (Starts 3.9)
deactivate
```

```
pip3 install django
virtualenv thor 
cd thor
source bin/activate
pip3 install django
django-admin startproject personal_porfolio
mv personal_porfolio/ personal_porfolio_project
cd personal_porfolio_project/
python3 manage.py startapp blog
python3 manage.py startapp portfolio
deactivate
```

#### Video Series 

OReilly Video Course

Django 3 - Full Stack Websites with Python Web Development
https://learning.oreilly.com/videos/django-3/9781801818148/

8 Hours of video.  I finished first two sections

Gave me a great insight in what Django is and how it works.  

Instructors Site: https://zappycode.com/

Excellent References from Course:

```
https://getbootstrap.com/docs/5.1/getting-started/introduction/
https://fonts.google.com/
```

#### Kubernetes Thoughts

- Database Will need to be persistent 
- MEDIA_ROOT for uploaded content will need to be persistent 
